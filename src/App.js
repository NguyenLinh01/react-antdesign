import React from 'react';
import logo from './logo.svg';
import './App.css';
import HomeBuilder from './containers/HomeBuilder/HomeBuilder';
function App() {
  return (
    <div className="App">
      <HomeBuilder />
    </div>
  );
}

export default App;
