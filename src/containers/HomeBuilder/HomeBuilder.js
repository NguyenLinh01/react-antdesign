import React from 'react';
import Menu from '../../components/Header/Menu';

import Side from '../../components/Header/Side';
import Product from '../../components/Body/Product';
import Voucher from '../../components/Body/Voucher';
import BestSales from '../../components/Body/BestSales';
import Hr from '../../components/Body/Hr';
import FooterContent from '../../components/Footer/Footer';
class HomeBuilder extends React.Component {
    state = {
        Button: {
            Login: "Đăng nhập",
            Account: "Tài khoản",
            Location: "Tây Ninh"
        },
        Product: [{
            id: "1",
            Price: "80.000đ + 0.01 OC hoặc 100k",
            Sold: "Đã bán 1k+"
        }],
        Voucher: [{
            id: "1",
            Name: "Category Name",
            Amount: "1k sản phẩm"
        }],
        BestSales: {
            ViewAll: "Xem tất cả >>",
            BestSales: "Bán chạy nhất"
        },
        FooterContent: {
            Title: {
                TakeCare: "CHĂM SÓC KHÁCH HÀNG",
                Mart: "VỀ ONES MART",
                Link: "HỢP TÁC VÀ LIÊN KẾT",
                ThanhToan: "PHƯƠNG THỨC THANH TOÁN",
                Connect: "KẾT NỐI VỚI CHÚNG TÔI"
            },
            Content: {
                Hotline1: "Hotline đặt hàng: 1800-6963 (Miễn phí , 8-21h kể cả T7, CN)",
                Hotline2: "Hotline chăm sóc khách hàng: 1900-6035 (1000đ/phút , 8-21h kể cả T7, CN)",
                Question: "Các câu hỏi thường gặp Gửi yêu cầu hỗ trợ Hướng dẫn đặt hàng Phương thức vận chuyển Chính sách đổi trả Hướng dẫn mua trả góp Chính sách hàng nhập khẩu Hỗ trợ khách hàng: support@tiki.vn"
                , Security: "Báo lỗi bảo mật: security@tiki.vn"
            }
        },
        showVoucher: true,
        showProduct: true,
    }
    render() {
        let product = null
        if (this.state.showProduct) {
            product = (
                <div>
                    {this.state.Product.map((x, index) => {
                        return <Product Price={x.Price} Sold={x.Sold} key={x.id} />
                    })}
                </div>
            );
        }
        let voucher = null
        if (this.state.showVoucher) {
            voucher = (
                <div>
                    {this.state.Voucher.map((x, index) => {
                        return <Voucher Name={x.Name} Amount={x.Amount} key={x.id} />
                    })}
                </div>
            );
        }
        return (
            <div>
                <div className="Header">
                    <Menu Login={this.state.Button.Login} Account={this.state.Button.Account} Location={this.state.Button.Location} />
                    <Side />
                </div>
                <div>
                    {product}
                    {voucher}
                    <BestSales BestSales={this.state.BestSales.BestSales} View={this.state.BestSales.ViewAll} />
                    {product}
                    <BestSales BestSales={this.state.BestSales.BestSales} View={this.state.BestSales.ViewAll} />
                    {product}
                    <BestSales BestSales={this.state.BestSales.BestSales} View={this.state.BestSales.ViewAll} />
                    {product}
                    <Hr />
                </div>
                <div>
                    <FooterContent
                        TakeCare={this.state.FooterContent.Title.TakeCare}
                        Mart={this.state.FooterContent.Title.Mart}
                        Link={this.state.FooterContent.Title.Link}
                        ThanhToan={this.state.FooterContent.Title.ThanhToan}
                        Connect={this.state.FooterContent.Title.Connect}
                        Hotline1={this.state.FooterContent.Content.Hotline1}
                        Hotline2={this.state.FooterContent.Content.Hotline2}
                        Question={this.state.FooterContent.Content.Question}
                        Security={this.state.FooterContent.Content.Security}
                    />
                </div>
            </div>
        );
    }
}
export default HomeBuilder;