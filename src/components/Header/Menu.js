import { Row, Col, Avatar, Input, Button } from 'antd';
import 'antd/dist/antd.css';
import React from 'react';
import '../../assets/css/home.css';
import img from '../../assets/Images/logo.png'

const headerComponent = (props) => {
    return (
        <div>
            <Row className="Menu" mode="horizontal" style={{ lineHeight: '64px' }}>
                <Col md={2} sx={1} sm={1} lg={1} xl={1} className="Logo" ><Avatar src={img} size={60} /></Col>
                <Col md={11} sx={24} sm={24} lg={12} xl={12}><Input placeholder="" /></Col>
                <Col md={5} sx={24} sm={24} lg={5} xl={5}><Input placeholder={props.Location} className="MarginInput" /></Col>
                <Col md={3} sx={12} sm={12} lg={2} xl={2}><Button className="btnLogin" type="link" >{props.Login}</Button></Col>
                <Col md={3} sx={12} sm={12} lg={1} xl={1}><Button className="btnLogin" type="link" >{props.Account}</Button></Col>
            </Row>
        </div>
    );
}
export default headerComponent;