import React from 'react';
import { Col, Row } from 'antd';
const footer = (props) => {
    return (
        <Row className="footer">
            <Col className="FooterContent" xl={5} lg={5} md={5} sm={24} sx={24}>
                <b>{props.TakeCare}</b>
                <p>{props.Hotline1}</p>
                <p>{props.Hotline2}</p>
                <p>{props.Question}</p>
                <p>{props.Security}</p>
            </Col>
            <Col className="FooterContent" xl={5} lg={5} md={5} sm={24} sx={24}><b>{props.Mart}</b></Col>
            <Col className="FooterContent" xl={5} lg={5} md={4} sm={24} sx={24}><b>{props.Link}</b></Col>
            <Col className="FooterContent" xl={4} lg={4} md={4} sm={24} sx={24}><b>{props.ThanhToan}</b></Col>
            <Col className="FooterContent" xl={4} lg={4} md={4} sm={24} sx={24}><b>{props.Connect}</b></Col>
        </Row>
    );
}
export default footer;