import React from 'react';
import {Row, Col, Typography } from 'antd';
import img from '../../assets/Images/voucher.png';
const { Text } = Typography;
const voucher = (props) => {
        return (
            <div className="ContentVoucher">
                <Row>
                    <Col lg={2} md={2} xl={2} sm={0} sx={0}></Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20} >
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={2} md={2} xl={2} sm={0} sx={0}></Col>
                </Row>
                <Row>
                    <Col lg={2} md={2} xl={2} sm={0} sx={0}></Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={2} md={2} xl={2} sm={0} sx={0}></Col>
                </Row>
                <Row>
                    <Col lg={2} md={2} xl={2} sm={0} sx={0}></Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5} lg={5} xl={5} sm={20} sx={20}>
                        <Row className="Voucher">
                            <Col span={10}>
                                <Text className="SizeText" type="warning">{props.Name}</Text><br></br>
                                <Text className="SizeText" type="secondary">{props.Amount}</Text>
                            </Col>
                            <Col span={12}>
                                <img alt="" src={img}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={2} md={2} xl={2} sm={0} sx={0}></Col>
                </Row>
            </div>    
        );
    }

export default voucher;