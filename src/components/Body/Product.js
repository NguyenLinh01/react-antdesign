import React from 'react';
import { Card, Row, Col, Typography } from 'antd';
import img from '../../assets/Images/Hotel_Nikko_saigon.png';
const { Text } = Typography;

const product = (props) => {
        return (
            <div className="ListProduct">
            <Row>
                <Col className="ColProduct" md={8} lg={8} xl={4} sm={24} sx={24}>
                    <Card className="CardProduct"
                        hoverable
                        style={{ width: 240 }}
                        cover={<img alt="example" src={img} />}>
                        <Row>
                            <Col className="DescriptionCard" md={14} lg={14} sm={24} sx={24} xl={14}>
                                <Text type="danger">{props.Price}</Text>
                            </Col>
                            <Col md={6} lg={6} sm={24} sx={24} xl={6}>
                                <Text type="warning">{props.Sold}</Text >
                            </Col>
                        </Row>
                        <Text className="Discount" type="danger" strong>30% <br></br> <Text type="secondary">GIẢM</Text></Text>
                    </Card>
                </Col>
                <Col className="ColProduct" md={8} lg={8} xl={4} sm={24} sx={24}>
                    <Card className="CardProduct"
                        hoverable
                        style={{ width: 240 }}
                        cover={<img alt="example" src={img} />}>
                        <Row>
                            <Col className="DescriptionCard"  md={14} lg={14} sm={24} sx={24} xl={14}>
                                <Text type="danger">{props.Price}</Text>
                            </Col>
                            <Col md={6} lg={6} sm={24} sx={24} xl={6}>
                                <Text type="warning">{props.Sold}</Text >
                            </Col>
                        </Row>
                        <Text className="Discount" type="danger" strong>30% <br></br> <Text type="secondary">GIẢM</Text></Text>
                    </Card>
                </Col>
                <Col className="ColProduct" md={8} lg={8} xl={4} sm={24} sx={24}>
                    <Card className="CardProduct"
                        hoverable
                        style={{ width: 240 }}
                        cover={<img alt="example" src={img} />}>
                        <Row>
                            <Col className="DescriptionCard"  md={14} lg={14} sm={24} sx={24} xl={14}>
                                <Text type="danger">{props.Price}</Text>
                            </Col>
                            <Col md={6} lg={6} sm={24} sx={24} xl={6}>
                                <Text type="warning">{props.Sold}</Text >
                            </Col>
                        </Row>
                        <Text className="Discount" type="danger" strong>30% <br></br> <Text type="secondary">GIẢM</Text></Text>
                    </Card>
                </Col>
                <Col className="ColProduct" md={8} lg={8} xl={4} sm={24} sx={24}>
                    <Card className="CardProduct"
                        hoverable
                        style={{ width: 240 }}
                        cover={<img alt="example" src={img} />}>
                        <Row>
                            <Col className="DescriptionCard"  md={14} lg={14} sm={24} sx={24} xl={14}>
                                <Text type="danger">{props.Price}</Text>
                            </Col>
                            <Col md={6} lg={6} sm={24} sx={24} xl={6}>
                                <Text type="warning">{props.Sold}</Text >
                            </Col>
                        </Row>
                        <Text className="Discount" type="danger" strong>30% <br></br> <Text type="secondary">GIẢM</Text></Text>
                    </Card>
                </Col>
                <Col className="ColProduct" md={8} lg={8} xl={4} sm={24} sx={24}>
                    <Card className="CardProduct"
                        hoverable
                        style={{ width: 240 }}
                        cover={<img alt="example" src={img} />}>
                        <Row>
                            <Col className="DescriptionCard" md={14} lg={14} sm={24} sx={24} xl={14}>
                                <Text type="danger">{props.Price}</Text>
                            </Col>
                            <Col md={6} lg={6} sm={24} sx={24} xl={6}>
                                <Text type="warning">{props.Sold}</Text >
                            </Col>
                        </Row>
                        <Text className="Discount" type="danger" strong>30% <br></br> <Text type="secondary">GIẢM</Text></Text>
                    </Card>
                </Col>
                <Col className="ColProduct" md={8} lg={8} xl={4} sm={24} sx={24}>
                    <Card className="CardProduct"
                        hoverable
                        style={{ width: 240 }}
                        cover={<img alt="example" src={img} />}>
                        <Row>
                            <Col className="DescriptionCard"  md={14} lg={14} sm={24} sx={24} xl={14}>
                                <Text type="danger">{props.Price}</Text>
                            </Col>
                            <Col md={6} lg={6} sm={24} sx={24} xl={6}>
                                <Text type="warning">{props.Sold}</Text >
                            </Col>
                        </Row>
                        <Text className="Discount" type="danger" strong>30% <br></br> <Text type="secondary">GIẢM</Text></Text>
                    </Card>
                </Col>
            </Row>
            </div>
        );
    }
export default product;