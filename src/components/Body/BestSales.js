import React from 'react';
import { Typography, Row, Col } from 'antd';
const { Text } = Typography;
const bestSales = (props) => {
        return (
            <div className="BackGroundBestSales">
            <div className="ContentBestSales">
                <Row>
                    <Col className="BestSales" span={12}>
                        <Text type="danger">{props.BestSales}</Text>
                    </Col>
                    <Col className="ViewAll" span={12}>
                        <Text type="danger">{props.View}</Text>
                    </Col>
                </Row>
            </div>
            </div>
        );
    }

export default bestSales;